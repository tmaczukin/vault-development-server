log_level = "debug"

storage "file" {
  path = "/vault/data"
}

listener "tcp" {
  address = "0.0.0.0:8200"
  tls_disable = 1
}

max_lease_ttl = "1h"
default_lease_ttl = "1h"
ui = true

telemetry {
  disable_hostname = true
  prometheus_retention_time = "1m"
}

