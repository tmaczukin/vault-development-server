FROM golang:1.22.1-alpine3.19 AS builder

ENV CGO_ENABLED=0

COPY ./operator /src
WORKDIR /src

RUN go build -o /usr/local/bin/vault-operator ./cmd/vault-operator

FROM hashicorp/vault:1.15.6

RUN apk add -U bash

COPY --from=builder /usr/local/bin/vault-operator /usr/local/bin/vault-operator
COPY entrypoint-overwrite.sh /usr/local/bin/entrypoint-overwrite.sh

ENTRYPOINT ["entrypoint-overwrite.sh"]
