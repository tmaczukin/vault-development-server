#!/usr/bin/dumb-init /bin/sh

set -e
set -x

chown -R vault:vault /vault

touch /nohup.out

nohup vault-operator &
nohup docker-entrypoint.sh "$@" &

exec tail -f /nohup.out

