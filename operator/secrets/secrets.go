package secrets

import (
	"encoding/json"
	"fmt"
	"os"
)

type Secrets struct {
	Keys      []string
	RootToken string
}

type Storage interface {
	Store(secrets *Secrets) error
	Load() (*Secrets, error)
}

type fileStorage struct {
	path string
}

func NewFileStorage(path string) Storage {
	return &fileStorage{path: path}
}

func (fs *fileStorage) Store(secrets *Secrets) error {
	file, err := os.Create(fs.path)
	if err != nil {
		return fmt.Errorf("creating file %q: %w", fs.path, err)
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	err = encoder.Encode(secrets)
	if err != nil {
		return fmt.Errorf("encoding secrets to JSON: %w", err)
	}

	return nil
}

func (fs *fileStorage) Load() (*Secrets, error) {
	file, err := os.Open(fs.path)
	if err != nil {
		return nil, fmt.Errorf("opening file %q: %w", fs.path, err)
	}
	defer file.Close()

	secrets := new(Secrets)

	decoder := json.NewDecoder(file)
	err = decoder.Decode(secrets)
	if err != nil {
		return nil, fmt.Errorf("decoding secrets from JSON: %w", err)
	}

	return secrets, nil
}
