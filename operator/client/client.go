package client

import (
	"fmt"

	"github.com/hashicorp/vault/api"

	"vault-operator/secrets"
)

type SealStatus struct {
	Sealed   bool
	Progress int
}

type Client struct {
	internal *api.Client
}

func New(url string) (*Client, error) {
	cfg := &api.Config{
		Address: url,
	}

	client, err := api.NewClient(cfg)
	if err != nil {
		return nil, fmt.Errorf("creating new Vault API client: %w", err)
	}

	c := &Client{
		internal: client,
	}

	return c, nil
}

func (c *Client) IsInitialized() (bool, error) {
	return c.internal.Sys().InitStatus()
}

func (c *Client) Initialize(shares int, threshold int) (*secrets.Secrets, error) {
	request := &api.InitRequest{
		SecretShares:    shares,
		SecretThreshold: threshold,
	}

	response, err := c.internal.Sys().Init(request)
	if err != nil {
		return nil, fmt.Errorf("initializing Vault: %w", err)
	}

	s := &secrets.Secrets{
		Keys:      response.Keys,
		RootToken: response.RootToken,
	}

	return s, nil
}

func (c *Client) Unseal(key string) (*SealStatus, error) {
	response, err := c.internal.Sys().Unseal(key)
	if err != nil {
		return nil, fmt.Errorf("unsealing Vault: %w", err)
	}

	r := &SealStatus{
		Sealed:   response.Sealed,
		Progress: response.Progress,
	}

	return r, nil
}
