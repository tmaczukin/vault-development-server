package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	"github.com/mitchellh/go-homedir"
	"github.com/sirupsen/logrus"

	"vault-operator/client"
	"vault-operator/secrets"
)

const (
	initializationSecretsFile = "/vault/config/secrets.json"
)

func main() {
	logger := logrus.New()
	secretsStorage := secrets.NewFileStorage(initializationSecretsFile)

	logger.Info("Creating new Vault client...")

	cli, err := client.New(os.Getenv("VAULT_ADDR"))
	if err != nil {
		logger.WithError(err).Fatal("Couldn't create new Vault API Client")
	}

	var isInitialized bool

	logger.Info("Checking Vault status...")
	for i := 0; i < 15; i++ {
		fmt.Print(".")
		isInitialized, err = cli.IsInitialized()
		if err == nil {
			break
		}

		time.Sleep(4 * time.Second)
	}
	fmt.Println()

	if err != nil {
		logger.WithError(err).Fatal("Couldn't get init status")
	}

	if !isInitialized {
		logger.Info("Initializing Vault...")
		s, err := cli.Initialize(5, 3)
		if err != nil {
			logger.WithError(err).Fatal("Couldn't initialize Vault")
		}

		logger.Info("Storing initialization secrets...")
		err = secretsStorage.Store(s)
		if err != nil {
			logger.WithError(err).Fatal("Couldn't store Vault initialization secrets")
		}

		logger.Info("Vault initialization secrets stored")
	}

	logger.Info("Loading initialization secrets...")
	s, err := secretsStorage.Load()
	if err != nil {
		logger.WithError(err).Fatal("Couldn't load Vault initialization secrets")
	}

	for id, key := range s.Keys {
		logger.WithField("key", id+1).Info("Unsealing Vault...")
		status, err := cli.Unseal(key)
		if err != nil {
			logger.WithError(err).Fatal("Couldn't unseal Vault with provided key")
		}

		logger.WithField("progress", status.Progress).WithField("sealed", status.Sealed).Info("Unseal secret accepted")
	}

	homeDir, err := homedir.Dir()
	if err != nil {
		homeDir = os.Getenv(".")
	}
	tokenFile := filepath.Join(homeDir, ".vault-token")
	_ = ioutil.WriteFile(tokenFile, []byte(s.RootToken), 0600)

	logger.WithField("root-token", s.RootToken).WithField("token-file", tokenFile).Info("Vault unsealed and ready to operate!")
}
