# Hashicorp Vault server development deployment

Development deployment of Hashicorp Vault server.

**This deployment is not a secure configuration that could be used for
a production deployment!**

In short `docker-compose up -d` should be enough to work. However, the configuration
is prepared to work in pair with my [local Traefik deployment](https://gitlab.com/tmaczukin/apollo-traefik). Without
it, `docker-compose up` will most probably fail.

If you don't want to follow my Traefik setup, then:

1. Remove the `traefik_webgateway` network definition from `networks:` section.
1. Remove the `web` network from `services:vault:network:` list.
1. After starting the deployment with `docker-compose up -d` run `docker inspect vault_vault_1` and find
   the IP of the container. Vault's UI will be accessible at `http://[IP]:8200/`.

## Author

Tomasz Maczukin, 2020

## License

MIT

